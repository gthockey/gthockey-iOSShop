//
//  ShopList.swift
//  
//
//  Created by Caleb Rudnicki on 2/22/22.
//

import SwiftUI
import GTHockeyUtils
import SDWebImageSwiftUI

public struct ShopList: View {
    
    @State private var apparel: [SimpleApparel] = []
    @State private var selectedApparel: SimpleApparel?
    
    private let layout: [GridItem] = Array(repeating: .init(.flexible()), count: 2)
    
    private let endpoint: String
    
    public init(from endpoint: String) {
        self.endpoint = endpoint
    }
    
    public var body: some View {
        ScrollView {
            LazyVGrid(columns: layout, spacing: 20) {
                ForEach(apparel) { apparel in
                    CustomGridCell() {
                        VStack(alignment: .leading, spacing: 10) {
//                            GeometryReader { geo in
                                WebImage(url: apparel.imageURL)
                                    .resizable()
                                    .indicator(.activity)
                                    .transition(.fade)
                                    .scaledToFill()
                                    .padding(.horizontal, 10)
                                    .frame(width: 145, height: 145)
                                    .clipped()
                                    .cornerRadius(5)
//                            }
//                                .padding([.top, .horizontal], 10)
                            VStack(alignment: .leading, spacing: 5) {
                                CustomText(apparel.name, font: .normalTextSemibold)
                                    .lineLimit(2)
                                CustomText(apparel.price.priceString, font: .smallCaption)
                            }
                            .frame(height: 72, alignment: .top)
                            Spacer()
                        }
                    }
                    .padding(.horizontal, 10)
                    .onTapGesture {
                        selectedApparel = apparel
                    }
                }
            }
            .padding(.horizontal, 10)
        }
        .onAppear { fetchApparel() }
        .fullScreenCover(item: $selectedApparel) { apparel in
            ApparelView(endpoint: endpoint, apparelID: apparel.id)
        }
        .navigationTitle("Shop")
    }
    
    private func fetchApparel() {
        ShopManager.shared.getApparel(from: endpoint, completion: { result in
            switch result {
            case .success(let apparel):
                self.apparel = apparel
            case .failure(let error):
                // TODO: Present an error to the user
                print(error.localizedDescription)
            }
        })
    }
    
}

extension Double {
    
    /// Converts a `Double` into a string representing its price
    var priceString: String {
        "$" + String(format: "%.2f", self)
    }
    
}

//struct ShopList_Previews: PreviewProvider {
//    static var previews: some View {
//        ShopList()
//    }
//}
