//
//  QuantityView.swift
//  
//
//  Created by Caleb Rudnicki on 2/28/22.
//

import SwiftUI
import GTHockeyUtils

internal struct QuantityView: View {
    
    @State private var customQuantity: String = ""
    @State private var selectedQuantity: Int = 1
    
    @ObservedObject internal var cartCoordinator: CartCoordinator
    
    private var presetQuantityOptions = [1, 2, 3]
    
    internal init(cartCoordinator: CartCoordinator) {
        self.cartCoordinator = cartCoordinator
    }
    
    internal var body: some View {
        VStack(alignment: .leading, spacing: 10) {
            CustomText("Quantity", font: .normalText)

            HStack {
                ForEach(presetQuantityOptions, id: \.self) { quantityOption in
                    Button(action: {
                        cartCoordinator.quantity = quantityOption
//                        selectedQuantity = quantityOption
                        customQuantity = ""
                    }, label: {
                        Text(String(describing: quantityOption))
                            .foregroundColor(cartCoordinator.quantity == quantityOption ? .black : .text)
                            .font(.normalText)
                            .padding()
                    })
                        .background(cartCoordinator.quantity == quantityOption && customQuantity.isEmpty ? Color.gold : .clear)
                        .cornerRadius(5)
                        .overlay(
                            RoundedRectangle(cornerRadius: 5)
                                .stroke(Color.text, lineWidth: 1)
                        )
                }
                
                TextField("Enter Quantity", text: $customQuantity)
                    .font(.normalText)
                    .padding()
                    .overlay(
                        RoundedRectangle(cornerRadius: 5)
                            .stroke(Color.text, lineWidth: 1)
                    )
                    .keyboardType(.numberPad)
                    .onChange(of: customQuantity) {
                        if let customQuantityInt = Int($0) {
                            cartCoordinator.quantity = customQuantityInt
                        }
                    }
            }
        }
    }
    
}

//struct QuantityView_Previews: PreviewProvider {
//    static var previews: some View {
//        QuantityView()
//    }
//}
