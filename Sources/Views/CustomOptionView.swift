//
//  CustomOptionView.swift
//  
//
//  Created by Caleb Rudnicki on 2/23/22.
//

import SwiftUI
import GTHockeyUtils

internal struct CustomOptionView: View {
    
    @State private var value: String = ""
    internal var option: CustomOption
    @ObservedObject internal var cartCoordinator: CartCoordinator
        
    internal var body: some View {
        VStack(alignment: .leading, spacing: 10) {
            CustomText(option.displayName + "\(option.required ? " (Required)" : "")", font: .normalText)
            TextField(option.helpText + " (\(option.extraCost.priceString) extra)", text: $value)
                .font(.normalText)
                .padding()
                .overlay(
                    RoundedRectangle(cornerRadius: 5)
                        .stroke(Color.text, lineWidth: 1)
                )
        }
        .onAppear {
            cartCoordinator.customOptions[option] = ""
        }
        .onChange(of: value) {
            print($0)
            cartCoordinator.customOptions[option] = $0
        }
    }
}

//struct CustomOptionView_Previews: PreviewProvider {
//    static var previews: some View {
//        CustomOptionView()
//    }
//}
