//
//  RestrictedOptionView.swift
//  
//
//  Created by Caleb Rudnicki on 2/23/22.
//

import SwiftUI
import GTHockeyUtils

internal struct RestrictedOptionView: View {
    
    internal var option: RestrictedOption
    @ObservedObject internal var cartCoordinator: CartCoordinator
    
    @State var selectedOption: String = ""
    
    private let layout: [GridItem] = [GridItem(.adaptive(minimum: 60))]
    
    internal var body: some View {
        VStack(alignment: .leading, spacing: 10) {
            CustomText(option.displayName, font: .normalText)
            
            let selections = option.optionsList.components(separatedBy: [","])
            
            LazyVGrid(columns: layout, alignment: .leading) {
                ForEach(selections, id: \.self) { selection in
                    Button(action: {
                        selectedOption = selection
                        cartCoordinator.restrictedOptions[option] = selectedOption
                    }, label: {
                        Text(selection)
                            .foregroundColor(selectedOption == selection ? .black : .text)
                            .font(.normalText)
                            .frame(maxWidth: .infinity)
                            .padding()
                    })
                        .background(selectedOption == selection ? Color.gold : .clear)
                        .cornerRadius(5)
                        .overlay(
                            RoundedRectangle(cornerRadius: 5)
                                .stroke(Color.text, lineWidth: 1)
                        )
                }
                
            }
            .onAppear {
                selectedOption = selections.first ?? ""
                cartCoordinator.restrictedOptions[option] = selectedOption
            }
        }
    }
}

//struct RestrictedOptionView_Previews: PreviewProvider {
//    static var previews: some View {
//        RestrictedOptionView()
//    }
//}
