//
//  ApparelView.swift
//  
//
//  Created by Caleb Rudnicki on 2/23/22.
//

import SwiftUI
import SDWebImageSwiftUI
import GTHockeyUtils

internal struct ApparelView: View {
    
    @Environment(\.presentationMode) var presentationMode
    
    internal var endpoint: String
    internal var apparelID: Int
    
    @State private var apparel: ComplexApparel? {
        didSet {
            cartCoordinator.basePrice = apparel?.price ?? 0
        }
    }
    @State private var bodyContentHeight: CGFloat = 0
    @State private var imageUrls = [URL]()
    
    @ObservedObject private var cartCoordinator = CartCoordinator()
    
    internal var body: some View {
        
        ZStack(alignment: .topTrailing) {
            ScrollView {
                if let apparel = apparel {
                    if !imageUrls.isEmpty {
                        GeometryReader { geo in
                            ImageCarousel(imageUrls: imageUrls)
                                .frame(width: geo.size.width)
                        }
                        .frame(height: 350)
                    }
                    
                    CustomText(apparel.name, font: .heading3)
                        .lineLimit(3)
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding(.bottom, 5)
                        .padding(.horizontal, 15)
                    
                    CustomText(apparel.price.priceString, font: .standardCaption)
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding(.bottom, 20)
                        .padding(.horizontal, 15)
                    
                    HTMLText(apparel.description, font: .lightText, height: $bodyContentHeight)
                            .frame(height: bodyContentHeight)
                            .padding(.bottom, 30)
                            .padding(.horizontal, 15)
                    
                    ForEach(apparel.restrictedOptions) { restrictedOption in
                        RestrictedOptionView(option: restrictedOption, cartCoordinator: cartCoordinator)
                            .padding(.bottom, 20)
                            .padding(.horizontal, 15)
                    }
                    
                    ForEach(apparel.customOptions) { customOption in
                        CustomOptionView(option: customOption, cartCoordinator: cartCoordinator)
                            .padding(.bottom, 20)
                            .padding(.horizontal, 15)
                    }
                    
                    QuantityView(cartCoordinator: cartCoordinator)
                        .padding(.bottom, 30)
                        .padding(.horizontal, 15)
                    
                    Button(action: {
                        cartCoordinator.addToCart()
                    }, label: {
                        Text("Add to Cart")
                            .foregroundColor(.black)
                            .font(.mediumTextSemibold)
                            .frame(maxWidth: .infinity)
                            .padding()
                    })
                        .background(cartCoordinator.addToCartDisabled ? Color.gray : Color.gold)
                        .disabled(cartCoordinator.addToCartDisabled)
                        .cornerRadius(5)
                        .shadow(color: .text, radius: 2)
                        .padding(.horizontal, 15)
                        .padding(.bottom, 30)
                }
            }
            .edgesIgnoringSafeArea(.top)
            
            Button(action: {
                presentationMode.wrappedValue.dismiss()
            }) {
                Image(systemName: "xmark.circle.fill")
            }
            .font(.heading3)
            .background(Color.navy)
            .clipShape(Circle())
            .foregroundColor(.powder)
            .padding([.top, .trailing])
        }
        .onAppear {
            fetchApparelItem()
        }
        .background(Color.background.edgesIgnoringSafeArea(.vertical))
    }
    
    private func fetchApparelItem() {
        ShopManager.shared.getApparel(from: endpoint, with: apparelID, completion: { result in
            switch result {
            case .success(let apparel):
                self.apparel = apparel
                var imageUrls: [URL] = [apparel.imageURL]
                for secondaryImage in apparel.secondaryImages {
                    imageUrls.append(secondaryImage.imageURL)
                }
                self.imageUrls = imageUrls
            case .failure(let error):
                // TODO: Present an error to the user
                print(error.localizedDescription)
            }
        })
    }
}

//struct ApparelView_Previews: PreviewProvider {
//    static var previews: some View {
//        ApparelView()
//    }
//}
