//
//  ShopManager.swift
//  
//
//  Created by Caleb Rudnicki on 2/23/22.
//

import Foundation
import GTHockeyUtils

/// The networking layer class for fetching a shop page
internal class ShopManager {
    
    internal static var shared = ShopManager()
    
    /// Fetches list of simple apparel items with a given url endpoint
    /// - Parameters:
    ///   - urlString: a `String` that represents the endpoint to reach out to
    ///   - completion: a completion type of either `.success([SimpleApparel])` or `.failure(NetworkingError)` indicating
    ///   the results of the fetch
    internal func getApparel(from urlString: String, completion: @escaping (Result<[SimpleApparel], NetworkingError>) -> Void) {
        guard let url = URL(string: urlString) else {
            completion(.failure(.invalidUrl))
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { data, _, error in
            guard let data = data else {
                completion(.failure(.invalidData))
                return
            }
            
            do {
                let apparel = try JSONDecoder().decode([SimpleApparel].self, from: data)
                completion(.success(apparel))
            } catch {
                completion(.failure(.parsingError))
            }
        }
        task.resume()
    }
    
    // Fetches list of complex apparel items with a given url endpoint
    /// - Parameters:
    ///   - urlString: a `String` that represents the endpoint to reach out to
    ///   - completion: a completion type of either `.success(ComplexApparel)` or `.failure(NetworkingError)` indicating
    ///   the results of the fetch
    internal func getApparel(from urlString: String, with apparelID: Int, completion: @escaping (Result<ComplexApparel, NetworkingError>) -> Void) {
        guard let url = URL(string: "\(urlString)\(apparelID)") else {
            completion(.failure(.invalidUrl))
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { data, _, error in
            guard let data = data else {
                completion(.failure(.invalidData))
                return
            }
            
            do {
                let apparelItem = try JSONDecoder().decode(ComplexApparel.self, from: data)
                completion(.success(apparelItem))
            } catch let error {
                print(error)
                completion(.failure(.parsingError))
            }
        }
        task.resume()
    }
    
}

