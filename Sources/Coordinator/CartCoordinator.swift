//
//  CartCoordinator.swift
//  
//
//  Created by Caleb Rudnicki on 2/28/22.
//

import Foundation

public class CartCoordinator: ObservableObject {
    
    // MARK: Published Variables
    
    @Published var restrictedOptions: [RestrictedOption : String] = [:]
    @Published var customOptions: [CustomOption : String] = [:] {
        didSet {
            for (key, value) in customOptions {
                if key.required && value.isEmpty {
                    addToCartDisabled = true
                    return
                }
            }
            addToCartDisabled = false
        }
    }
    @Published var quantity: Int = 1
    @Published var addToCartDisabled: Bool = false
    
    var basePrice: Double = 0
        
    // MARK: Public Functions
    
    func addToCart() {
        print("Adding to cart")
        
        var total = basePrice
        
        print("Restricted Options")
        for (key, value) in restrictedOptions {
            print(key.displayName + ": " + value)
        }
        
        print("Custom Options")
        for (key, value) in customOptions {
            if !value.isEmpty {
                print(key.displayName + ": " + value + ". Required: \(key.required). Added Cost: \(key.extraCost)")
                total += key.extraCost
            } else {
                print("Not Selected Option: " + key.displayName + ": " + value + ". Required: \(key.required). Added Cost: \(key.extraCost)")
            }
        }
        
        print("Quantity: \(quantity)")
        print("Total Price: \(total * Double(quantity))")
    }
    
}
