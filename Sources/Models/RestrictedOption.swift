//
//  RestrictedOption.swift
//  
//
//  Created by Caleb Rudnicki on 2/23/22.
//

import Foundation

internal struct RestrictedOption: Identifiable, Decodable, Hashable {
    
    let id: Int
    let displayName: String
    let helpText: String
    let optionsList: String
    let correspondingApparelID: Int
    
    enum CodingKeys: String, CodingKey {
        case displayName = "display_name"
        case helpText = "help_text"
        case optionsList = "option_list"
        case correspondingApparelID = "shop_item"
        case id
    }
    
}
