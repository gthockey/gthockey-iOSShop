//
//  SecondaryImage.swift
//  
//
//  Created by Caleb Rudnicki on 2/23/22.
//

import Foundation

internal struct SecondaryImage: Identifiable, Decodable {
     
    let id: Int
    let imageURL: URL
    let correspondingApparelID: Int
    
    enum CodingKeys: String, CodingKey {
        case imageURL = "image"
        case correspondingApparelID = "shop_item"
        case id
    }
    
}
