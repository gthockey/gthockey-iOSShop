//
//  Apparel.swift
//  
//
//  Created by Caleb Rudnicki on 2/22/22.
//

import Foundation

/// An object to represent a single apparel item with only surface level data
internal struct SimpleApparel: Identifiable, Decodable {
    
    let id: Int
    let name: String
    let price: Double
    let description: String
    let imageURL: URL
    
    enum CodingKeys: String, CodingKey {
        case imageURL = "image"
        case id, name, price, description
    }
    
}
