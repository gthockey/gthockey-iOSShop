//
//  CustomOption.swift
//  
//
//  Created by Caleb Rudnicki on 2/23/22.
//

import Foundation

internal struct CustomOption: Identifiable, Decodable, Hashable {
    
    let id: Int
    let displayName: String
    let helpText: String
    let required: Bool
    let extraCost: Double
    let correspondingApparelID: Int
    
    enum CodingKeys: String, CodingKey {
        case displayName = "display_name"
        case helpText = "help_text"
        case extraCost = "extra_cost"
        case correspondingApparelID = "shop_item"
        case id, required
    }
    
}
