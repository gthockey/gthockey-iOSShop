//
//  ComplexApparel.swift
//  
//
//  Created by Caleb Rudnicki on 2/23/22.
//

import Foundation

/// An object to represent a single apparel item with only all internal data
internal struct ComplexApparel: Identifiable, Decodable {
    
    let id: Int
    let name: String
    let price: Double
    let description: String
    let imageURL: URL
    let secondaryImages: [SecondaryImage]
    let restrictedOptions: [RestrictedOption]
    let customOptions: [CustomOption]
    let inStock: Bool
    
    enum CodingKeys: String, CodingKey {
        case imageURL = "image"
        case secondaryImages = "images"
        case restrictedOptions = "options"
        case customOptions = "custom_options"
        case inStock = "in_stock"
        case id, name, price, description
    }
    
}
