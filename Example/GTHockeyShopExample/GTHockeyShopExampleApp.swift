//
//  GTHockeyShopExampleApp.swift
//  GTHockeyShopExample
//
//  Created by Caleb Rudnicki on 2/22/22.
//

import SwiftUI
import GTHockeyShop

@main
struct GTHockeyShopExampleApp: App {
    var body: some Scene {
        WindowGroup {
            NavigationView {
                ShopList(from: "http://127.0.0.1:8000/api/shop/").background(Color.background)
            }
        }
    }
}
